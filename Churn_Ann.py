# Artificial NN

# Task 1: Data Preprocessing


# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values


# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X1 = LabelEncoder()
X[:, 1] = labelencoder_X1.fit_transform(X[:, 1])
labelencoder_X2 = LabelEncoder()
X[:, 2] = labelencoder_X2.fit_transform(X[:, 2])

onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:,1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Task 2: Make ANN

# Import Keras and packages

import keras
from keras.models import Sequential
from keras.layers import Dense

# Initialising the ANN
classifier = Sequential()

# Adding the input layer and first hidden layer
# units: no of hidden layer = (input + output)/2 =11+1/2=6
# kernel_initializer: initialize the weights with smallest value- SGD 
# activation : Rectifier function
# input_dim :independent variable
classifier.add(Dense(activation="relu", input_dim=11, units=6, kernel_initializer="uniform"))

# Adding Second hidden layer
classifier.add(Dense(activation="relu", units=6, kernel_initializer="uniform"))

# Adding the output layer
classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))

#Compiling the ANN-
#optimizer: stochastic gradient descent on ANN - adam
#loss: 2 category binary_crossentropy/ 3 category- categorical_crossentropy
#metrics: use to evalute the model
classifier.compile(optimizer= 'adam', loss='binary_crossentropy', metrics=['accuracy'] )

# Fitting ANN to the Training set
#Epochs: no of times we re training our ANN oon the whole training set

# batch size- after how many observations we want to change the weights
classifier.fit(X_train,y_train,batch_size =10, nb_epoch=100)

# Task 3: Predict and evaluate the model
# Predicting the Test set results
y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)


# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

# Calculating Accurcy

from sklearn.metrics import accuracy_score
accuracy_score(y_test, y_pred)