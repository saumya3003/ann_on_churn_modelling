# Artificial NN

# Task 1: Data Preprocessing


# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values


# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X1 = LabelEncoder()
X[:, 1] = labelencoder_X1.fit_transform(X[:, 1])
labelencoder_X2 = LabelEncoder()
X[:, 2] = labelencoder_X2.fit_transform(X[:, 2])

onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:,1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Task 2: Make ANN

# Import Keras and packages

import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
# Initialising the ANN
classifier = Sequential()

# Adding the input layer and first hidden layer
# units: no of hidden layer = (input + output)/2 =11+1/2=6
# kernel_initializer: initialize the weights with smallest value- SGD 
# activation : Rectifier function
# input_dim :independent variable
classifier.add(Dense(activation="relu", input_dim=11, units=6, kernel_initializer="uniform"))

# Adding a dropout- P 0 to 1: fraction of the neutons you want to drop or disable at each itteration
classifier.add(Dropout(p=0.1))

# Adding Second hidden layer
classifier.add(Dense(activation="relu", units=6, kernel_initializer="uniform"))
classifier.add(Dropout(p=0.1))
# Adding the output layer
classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))

#Compiling the ANN-
#optimizer: stochastic gradient descent on ANN - adam
#loss: 2 category binary_crossentropy/ 3 category- categorical_crossentropy
#metrics: use to evalute the model
classifier.compile(optimizer= 'adam', loss='binary_crossentropy', metrics=['accuracy'] )

# Fitting ANN to the Training set
#Epochs: no of times we re training our ANN oon the whole training set

# batch size- after how many observations we want to change the weights     
classifier.fit(X_train,y_train,batch_size =10, nb_epoch=100)

# Task 3: Predict and evaluate the model
# Predicting the Test set results
y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)


# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

# Calculating Accurcy

from sklearn.metrics import accuracy_score
accuracy_score(y_test, y_pred)


# Task 4: Evaluate, Improve, Tune the ANN

# Evaluate ANN

# =============================================================================
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
import keras
from keras.models import Sequential
from keras.layers import Dense
 
# function to create a classifier
def create_classifier():
     #local classifier
    classifier = Sequential()
    classifier.add(Dense(activation="relu", input_dim=11, units=6, kernel_initializer="uniform"))
    classifier.add(Dense(activation="relu", units=6, kernel_initializer="uniform"))
    classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))
    classifier.compile(optimizer= 'adam', loss='binary_crossentropy', metrics=['accuracy'] )
    return classifier
 
# global classifier
classifier = KerasClassifier(build_fn = create_classifier,batch_size =10, epochs=100)
 
# cross_val_score will fit the model on k folds and return k accuracies
# estimator - object sed to fit the data
# x- data to train X_train
# y- target y_train
# cv = fold
#n_jobs: -1 run parallel computations
accuracies = cross_val_score(estimator = classifier, X = X_train, y= y_train,cv = 10, n_jobs= -1)

# Calculate the mean of accuracies
mean = accuracies.mean()

#calcuate the variance
variance = accuracies.std()

# =============================================================================
 
# Improve ANN
# Use Dropout Regularisation for avoiding overfitting

# Tune ANN

from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV
import keras
from keras.models import Sequential
from keras.layers import Dense
 
# function to create a classifier
def create_classifier(optimizer):
     #local classifier
    classifier = Sequential()
    classifier.add(Dense(activation="relu", input_dim=11, units=6, kernel_initializer="uniform"))
    classifier.add(Dense(activation="relu", units=6, kernel_initializer="uniform"))
    classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))
    classifier.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy'] )
    return classifier
 
# global classifier
classifier = KerasClassifier(build_fn = create_classifier)
# hyperparameters -batch_sze,epoch,optimizer
parameters = {'batch_size' : [25,32],
              'nb_epoch' : [100,500],
              'optimizer' : ['adam', 'rmsprop']}

grid_search = GridSearchCV(estimator = classifier, param_grid = parameters, scoring = 'accuracy',cv= 10)

grid_search = grid_search.fit(X_train,y_train)
best_para = grid_search.best_params_
best_acc = grid_search.best_score_
